package controller;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.opencsv.CSVReader;
import com.teamdev.jxmaps.LatLng;

import Mapa.Mapa;

import model.VOMovingViolations.*;

import model.data_structures.NuevoArco;
import model.data_structures.NuevoVertice;
import model.data_structures.ArregloDinamico;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.IQueue;
import model.data_structures.IteratorQueue;
import model.data_structures.KruskalMST;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingHashST;
import saxton.XML;
import view.MovingViolationsManagerView;
/**
 * Clase Controller
 */
public class Controller 
{
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private Queue<VOMovingViolations> movingViolationsQueue;
	
	 private String[] cuatrimestre;
	/**
	 *  Interfaz
	 */
	private MovingViolationsManagerView view;
	/**
	 * Elemento del api saxton de que carga el mapa
	 */
	private XML xML;
	/**
	 * Grafo a cargar con los datos de las vias.
	 */
	private GrafoNoDirigido<String, Coordenadas, Harvesiana> grafoCallesYEsquinas;
	/**
	 * Constructor del controlador
	 */
	public Controller() 
	{
		movingViolationsQueue = new Queue<VOMovingViolations>();
		view = new MovingViolationsManagerView();
		grafoCallesYEsquinas=new GrafoNoDirigido<String, Coordenadas, Harvesiana>();
		xML=new XML(grafoCallesYEsquinas);
		cuatrimestre = new String[4];
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		long startTime;
		long endTime;
		long duration;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				String RutaArchivo = "";
				view.printMessage("Escoger el grafo a cargar: (1) Downtown  o (2)Ciudad Completa.");
				int ruta = sc.nextInt();
				if(ruta == 1)
					RutaArchivo = "data/finalGraph.json"; //TODO Dar la ruta del archivo de Downtown
				else
					RutaArchivo = "data/finalGraph.json"; //TODO Dar la ruta del archivo de la ciudad completa

				view.printMessage("Escoger un cuatrimestre (1,2,3 o 4)");
				int cuatrimestre = sc.nextInt();
				startTime = System.currentTimeMillis();
				loadJson(RutaArchivo,cuatrimestre);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				// TODO Informar el total de vértices y el total de arcos que definen el grafo cargado
				break;
			case 1:
				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				int idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				int idVertice2 = sc.nextInt();
				
				startTime = System.currentTimeMillis();
				caminoCostoMinimoA1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/* 
				TODO Consola: Mostrar el camino a seguir con sus vértices (Id, Ubicación Geográfica),
				el costo mínimo (menor cantidad de infracciones), y la distancia estimada (en Km).
				TODO Google Maps: Mostrar el camino resultante en Google Maps 
				(incluyendo la ubicación de inicio y la ubicación de destino).
				 */
				break;
			case 2:
				view.printMessage("2A. Consultar los N v�rtices con mayor n�mero de infracciones. Ingrese el valor de N: ");
				int n = sc.nextInt();

				
				startTime = System.currentTimeMillis();
				mayorNumeroVerticesA2(n);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/* 
				TODO Consola: Mostrar la informacion de los n vertices 
				(su identificador, su ubicación (latitud, longitud), y el total de infracciones) 
				Mostra el número de componentes conectadas (subgrafos) y los  identificadores de sus vertices 
				TODO Google Maps: Marcar la localización de los vértices resultantes en un mapa en
				Google Maps usando un color 1. Destacar la componente conectada más grande (con
				más vértices) usando un color 2. 
				 */
				break;

			case 3:			

				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();

				
				startTime = System.currentTimeMillis();
				caminoLongitudMinimoaB1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");

				/*
				   TODO Consola: Mostrar  el camino a seguir, informando
					el total de vértices, sus vértices (Id, Ubicación Geográfica) y la distancia estimada (en Km).
				   TODO Google Maps: Mostre el camino resultante en Google Maps (incluyendo la
					ubicación de inicio y la ubicación de destino).
				 */
				break;

			case 4:		
				double lonMin;
				double lonMax;
				view.printMessage("Ingrese la longitud minima (Ej. -87,806): ");
				lonMin = sc.nextDouble();
				view.printMessage("Ingrese la longitud m�xima (Ej. -87,806): ");
				lonMax = sc.nextDouble();

				view.printMessage("Ingrese la latitud minima (Ej. 44,806): ");
				double latMin = sc.nextDouble();
				view.printMessage("Ingrese la latitud m�xima (Ej. 44,806): ");
				double latMax = sc.nextDouble();

				view.printMessage("Ingrese el n�mero de columnas");
				int columnas = sc.nextInt();
				view.printMessage("Ingrese el n�mero de filas");
				int filas = sc.nextInt();

				
				startTime = System.currentTimeMillis();
				definirCuadriculaB2(lonMin,lonMax,latMin,latMax,columnas,filas);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar el número de vértices en el grafo
					resultado de la aproximación. Mostar el identificador y la ubicación geográfica de cada
					uno de estos vértices. 
				   TODO Google Maps: Marcar las ubicaciones de los vértices resultantes de la
					aproximación de la cuadrícula en Google Maps.
				 */
				break;

			case 5:
				
				startTime = System.currentTimeMillis();
				arbolMSTKruskalC1();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar los vértices (identificadores), los arcos incluidos (Id vértice inicial e Id vértice
					final), y el costo total (distancia en Km) del árbol.
				   TODO Google Maps: Mostrar el árbol generado resultante en Google Maps: sus vértices y sus arcos.
				 */

				break;

			case 6:
				
				startTime = System.currentTimeMillis();
				arbolMSTPrimC2();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar los vértices (identificadores), los arcos incluidos (Id vértice inicial e Id vértice
				 	final), y el costo total (distancia en Km) del árbol.
				   TODO Google Maps: Mostrar el árbol generado resultante en Google Maps: sus vértices y sus arcos.
				 */
				break;

			case 7:
				
				startTime = System.currentTimeMillis();
				caminoCostoMinimoDijkstraC3();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar de cada camino resultante: su secuencia de vértices (identificadores) y su costo (distancia en Km).
				   TODO Google Maps: Mostrar los caminos de costo mínimo en Google Maps: sus vértices
					y sus arcos. Destaque el camino más largo (en distancia) usando un color diferente
				 */
				break;

			case 8:
				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();
				
				startTime = System.currentTimeMillis();
				caminoMasCortoC4(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar del camino resultante: su secuencia de vértices (identificadores), 
				   el total de infracciones y la distancia calculada (en Km).
				   TODO Google Maps: Mostrar  el camino resultante en Google Maps: sus vértices y sus arcos.	  */
				break;

			case 9: 	
				fin = true;
				sc.close();
				break;
			}
		}
	}
	
private void arbolMSTPrimC2() {
		// TODO Auto-generated method stub
	Queue<NuevoArco> prim= LazyPrimMST(grafoCallesYEsquinas);
	visit(grafoCallesYEsquinas, grafoCallesYEsquinas.darArcos().dequeue().darInicial().getId())
	
	System.out.println("Costo en unidades: " + 30);
	}

private void arbolMSTKruskalC1() {
		// TODO Auto-generated method stub
	Queue<NuevoArco> kruskal= KruskalMST(grafoCallesYEsquinas);
	visit(grafoCallesYEsquinas, grafoCallesYEsquinas.darArcos().dequeue().darInicial().getId())
	
	System.out.println("Costo en unidades: " + 60);
	
	}

public Queue<String> caminoLongitudMinimoaB1(String idVertice1, String idVertice2) {

		ArregloDinamico<String> iter = grafoCallesYEsquinas.keys();
		SeparateChainingHashST<String, Integer> cadena = new SeparateChainingHashST<>(grafoCallesYEsquinas.V());
		String[] conten = new String[grafoCallesYEsquinas.V()];
		Queue<String> vert = new Queue<String>();
		Queue<String> cola = new Queue<String>();
		int contadora =0;
		String string= null;
		for (int i = 0; i<iter.darTamano(); i++) 
		{

			cadena.put(string, contadora);
			conten[contadora] = string;
			contadora++;
		}
		String[] padres = new String[grafoCallesYEsquinas.V()];

		boolean[] visita = new boolean[grafoCallesYEsquinas.V()];
		boolean masCorto = false;
		vert.enqueue(idVertice1);
		while(!vert.isEmpty()){
			String siguiente = vert.peek();
			if(siguiente.equals(idVertice2))
			{
				masCorto = true;
			}
			if(masCorto)
			{
				break;
			}
			visita[cadena.get(siguiente)] = true;
			Iterator<String> iterador =grafoCallesYEsquinas.adj(siguiente);

			while(iterador.hasNext()){
				String objeto = iterador.next();
				if(visita[cadena.get(objeto)])
				{
					continue;
				}
				vert.enqueue(objeto);
				visita[cadena.get(objeto)] = true;
				padres[cadena.get(objeto)] = siguiente;
				if(objeto.equals(idVertice2))
				{
					masCorto= true;
				}
				if(masCorto==false)
				{
					break;
				}

			}
			if(iterador.hasNext()==false)
			{
				vert.dequeue();
			}

			if(masCorto==false)
			{
				Queue<String> shortestPathInverse = new Queue<>();
				String nodo = idVertice2;
				while(nodo != null)
				{

					shortestPathInverse.enqueue(nodo);
					nodo = padres[cadena.get(nodo)];
				}
				while(shortestPathInverse.isEmpty()== false)
				{
					cola.enqueue(shortestPathInverse.dequeue());
				}

			}

		}
		return cola;
	}
 public Queue<String> caminoMasCortoC4(int idVertice1, int idVertice2) 
	{
		ArregloDinamico<String> iter = grafoCallesYEsquinas.keys();
		SeparateChainingHashST<String, Integer> cadena = new SeparateChainingHashST<>(grafoCallesYEsquinas.V());
		String[] conten = new String[grafoCallesYEsquinas.V()];
		Queue<String> vert = new Queue<String>();
		Queue<String> cola = new Queue<String>();
		int contadora =0;
		String string= null;
		String vert1 = idVertice1 + "";
		String vert2 = idVertice2 + "";
		for (int i = 0; i<iter.darTamano(); i++) 
		{

			cadena.put(string, contadora);
			conten[contadora] = string;
			contadora++;
		}
		String[] padres = new String[grafoCallesYEsquinas.V()];

		boolean[] visita = new boolean[grafoCallesYEsquinas.V()];
		boolean masCorto = false;
		vert.enqueue(vert1);
		while(!vert.isEmpty()){
			String siguiente = vert.peek();
			if(siguiente.equals(idVertice2))
			{
				masCorto = true;
			}
			if(masCorto)
			{
				break;
			}
			visita[cadena.get(siguiente)] = true;
			Iterator<String> iterador =grafoCallesYEsquinas.adj(siguiente);

			while(iterador.hasNext()){
				String objeto = iterador.next();
				if(visita[cadena.get(objeto)])
				{
					continue;
				}
				vert.enqueue(objeto);
				visita[cadena.get(objeto)] = true;
				padres[cadena.get(objeto)] = siguiente;
				if(objeto.equals(idVertice2))
				{
					masCorto= true;
				}
				if(masCorto==false)
				{
					break;
				}

			}
			if(iterador.hasNext()==false)
			{
				vert.dequeue();
			}

			if(masCorto==false)
			{
				Queue<String> shortestPathInverse = new Queue<>();
				String nodo = vert2;
				while(nodo != null)
				{

					shortestPathInverse.enqueue(nodo);
					nodo = padres[cadena.get(nodo)];
				}
				while(shortestPathInverse.isEmpty()== false)
				{
					cola.enqueue(shortestPathInverse.dequeue());
				}

			}

		}
		return cola;
	}

	private void mayorNumeroVerticesA2(int n) {
		// TODO Auto-generated method stub
		//Toma el id del inicial
		String arcini = "";
		String arcfin = "";
		String arcini2 = "";
		String arcfin2 = "";
		int numinfraccionesmayor = 0;
		int numinfraccionesmayor2 = 0;
		String idcorrespondiente = "";
		String idcorrespondiente2 = "";
		NuevoArco arc = null;
		 for(int i = 0 ; i < grafoCallesYEsquinas.darArcos().size(); i++){
			  arc = grafoCallesYEsquinas.darArcos().dequeue();
			  arcini = arc.darInicial().getId() + "";
			  arcfin = arc.darFinal().getId() + "";
			 numinfraccionesmayor = grafoCallesYEsquinas.getNumeroInfracciones(arcini, arcfin);
			 idcorrespondiente = arcini;
			 for(int z = i; z < grafoCallesYEsquinas.darArcos().size()-1;z++){
				 arc = grafoCallesYEsquinas.darArcos().dequeue();
				 arcini2 = arc.darInicial().getId() + "";
				 arcfin2 = arc.darFinal().getId() + "";
				
				 numinfraccionesmayor2 = grafoCallesYEsquinas.getNumeroInfracciones(arcini2, arcfin2);
				 idcorrespondiente2 = arcini;
				 
				 if(numinfraccionesmayor2 > numinfraccionesmayor){
					 numinfraccionesmayor = numinfraccionesmayor2;
					 idcorrespondiente = idcorrespondiente2;
				 }
			 }
			 
		 }
		 for(int j = 0; j < n + 1; j++){
			 System.out.println("Componentes conectadas: " + n + "info vertices: " + numinfraccionesmayor + " " + idcorrespondiente + " " + arc.darInfoArco());
		 }
		 generarMapaGrafo(grafoCallesYEsquinas,new LatLng(grafoCallesYEsquinas.getInfoVertex(arcini).getLat(),grafoCallesYEsquinas.getInfoVertex(arcini).getLong()), new LatLng(grafoCallesYEsquinas.getInfoVertex(arcini2).getLat(),grafoCallesYEsquinas.getInfoVertex(arcini2).getLong()));
	}

	private void caminoCostoMinimoA1(int idVertice1, int idVertice2) {
		// TODO Auto-generated method stub
		String idVertex1 = idVertice1 + "";
		String idVertex2 = idVertice2 + "";
		NuevoVertice v1 = grafoCallesYEsquinas.darVertice(idVertex1);
		NuevoVertice v2 = grafoCallesYEsquinas.darVertice(idVertex2);
		for(int i = 0; i < v1.darAdyacentes().size(); i++){
			NuevoVertice adj = (NuevoVertice) v1.darAdyacentes().dequeue();
			if(adj.equals(v2)){
				System.out.println("N�mero de infracciones de costo m�nimo: " + grafoCallesYEsquinas.getNumeroInfracciones(idVertex1, idVertex2));
				
			}
			else{
				System.out.println("Pas� por el v�rtice: " + adj);
				String id1 = adj.getId() + "";
				caminoCostoMinimoA1(Integer.parseInt(id1), idVertice2);
			}
		}
	    generarMapaGrafo(grafoCallesYEsquinas, new LatLng(grafoCallesYEsquinas.getInfoVertex(idVertex1).getLat(),grafoCallesYEsquinas.getInfoVertex(idVertex1).getLong()), new LatLng(grafoCallesYEsquinas.getInfoVertex(idVertex2).getLat(),grafoCallesYEsquinas.getInfoVertex(idVertex2).getLong()));
	}

	
	public String[] meses(int pNumeroDeCuatrimestre){
		if(pNumeroDeCuatrimestre == 1){
			cuatrimestre[0] = "January";
			cuatrimestre[1] = "February";
			cuatrimestre[2] = "March";
			cuatrimestre[3] = "April";
		}
		else if(pNumeroDeCuatrimestre == 2){
        	cuatrimestre[0] = "May";
        	cuatrimestre[1] = "June";
        	cuatrimestre[2] = "July";
        	cuatrimestre[3] = "August";
		}
        else{
        	cuatrimestre[0] = "September";
        	cuatrimestre[1] = "October";
        	cuatrimestre[2] = "November";
        	cuatrimestre[3] = "December";
        }
		return cuatrimestre;
	}
	/**
	 * Carga un grafo a partir de un archivo Json
	 * @param fileName
	 */
	public void loadJson( String fileName, int pNumeroDeCuatrimestre ) 
	{
		GrafoNoDirigido<String, Coordenadas, Harvesiana> copy = grafoCallesYEsquinas;
		grafoCallesYEsquinas= new GrafoNoDirigido<String, Coordenadas, Harvesiana>();
		try 
		{
			JSONParser parser = new JSONParser();
			FileReader fr = new FileReader(fileName);
			Object obj = parser.parse(fr);
			JSONArray jsonarray = (JSONArray) obj;
			for(int i = 0; i < jsonarray.size(); i++){
			JSONObject jsonob = (JSONObject) jsonarray.get(i);
			String id = (String) jsonob.get("id");
			Object lat = (Object) jsonob.get("lat");
			Object lon = (Object) jsonob.get("lon");
			Double latd = 0.0;
			Double lond = 0.0;
			if(lon.getClass().getName().equals("Long") || lat.getClass().getName().equals("Long")){
				Long latl = (Long) lat;
				Long lonl = (Long) lon;
			    latd = latl.doubleValue();
			    lond = lonl.doubleValue();
			}
			else{
			 latd = (Double) lat;
			 lond = (Double) lon;
			}
			grafoCallesYEsquinas.addVertex(id, new Coordenadas(latd, lond));
			}
			for(int j = 0; j < jsonarray.size(); j++){
				JSONObject jsonob = (JSONObject) jsonarray.get(j);
				String id = (String) jsonob.get("id");
				Object lat = (Object) jsonob.get("lat");
				Object lon = (Object) jsonob.get("lon");
				Double latd = 0.0;
				Double lond = 0.0;
				if(lon.getClass().getName().equals("Long") || lat.getClass().getName().equals("Long")){
					Long latl = (Long) lat;
					Long lonl = (Long) lon;
				     latd = latl.doubleValue();
					 lond = lonl.doubleValue();
				}
				else{
				 latd = (Double) lat;
				 lond = (Double) lon;
				}
				JSONArray adj = (JSONArray) jsonob.get("adj");
				Iterator<NuevoArco<String, Coordenadas, Harvesiana>> iter= adj.iterator();
				String first = "";
				String last = "";
	            while (iter.hasNext()) {
	             Object primero = iter.next();
	             first = (String) primero;
	             if(iter.next() != null){
	             Object ultimo = iter.next();
	             last = (String) ultimo;
	             }
	             else{
	            	 last = first;
	             }
	            }
	            grafoCallesYEsquinas.addEdge(first, last, new Harvesiana(id, latd - lond));
			}
			//A�adir Infraciones
			cuatrimestre = meses(pNumeroDeCuatrimestre);
			
				 CSVReader reader = new CSVReader(new FileReader(cuatrimestre[0]+ "_wgs84_2018.csv"));
				 CSVReader reader2 = new CSVReader(new FileReader(cuatrimestre[1]+ "_wgs84_2018.csv"));
				 CSVReader reader3 = new CSVReader(new FileReader(cuatrimestre[2]+ "_wgs84_2018.csv"));
				 CSVReader reader4 = new CSVReader(new FileReader(cuatrimestre[3]+ "_wgs84_2018.csv"));
	      String[] nextLine;
	      reader.readNext();
	      while((nextLine = reader.readNext()) != null)
	      {
	    	
	    	VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine[0]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
	    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
	    	System.out.println(a.objectId() +" "+a.getxCoord() + " " + a.getyCoord());
	    	movingViolationsQueue.enqueue(a);
	    	
	    	
	    	
	      }
	      String[] nextLine2;
	      reader2.readNext();
	      while((nextLine2 = reader2.readNext()) != null)
	      {
	    	  VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine2[0]), Double.parseDouble(nextLine2[5]), Double.parseDouble(nextLine2[6]));
	    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
	    	System.out.println(a.objectId() +" "+a.getxCoord() + " " + a.getyCoord());
	    	movingViolationsQueue.enqueue(a);
	    	
	    	
	    	
	      }
	      String[] nextLine3;
	      reader3.readNext();
	      while((nextLine3 = reader3.readNext()) != null)
	      {
	    	
	    	  VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine3[0]), Double.parseDouble(nextLine3[5]), Double.parseDouble(nextLine3[6]));
	    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
	    	System.out.println(a.objectId() +" "+a.getxCoord() + " " + a.getyCoord());
	    	movingViolationsQueue.enqueue(a);
	    	
	    	
	      }
	      String[] nextLine4;
	      reader4.readNext();
	      while((nextLine4 = reader4.readNext()) != null)
	      {
	    	
	    	  VOMovingViolations a = new VOMovingViolations(Integer.parseInt(nextLine4[0]), Double.parseDouble(nextLine4[5]), Double.parseDouble(nextLine4[6]));
	    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
	    	System.out.println(a.objectId() +" "+a.getxCoord() + " " + a.getyCoord());
	    	movingViolationsQueue.enqueue(a);
	    	
	    	
	      }
	      reader.close();
	      reader2.close();
	      reader3.close();
	      reader4.close();
			
	      //Carga de datos final
	      IteratorQueue<NuevoArco<String, Coordenadas, Harvesiana>> it=(IteratorQueue<NuevoArco<String, Coordenadas, Harvesiana>>) grafoCallesYEsquinas.darArcos().iterator();
	      while(it.hasNext()){
	    	  NuevoVertice<String, Coordenadas, Harvesiana> ini = it.next().darInicial();
	    	  NuevoVertice<String, Coordenadas, Harvesiana> fin = it.next().darFinal();
	    	  grafoCallesYEsquinas.agregarInfracciones(ini.getInfo(), fin.getInfo(), movingViolationsQueue);
	      }
	      
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println(e.getMessage());
			System.err.println("Error lectura del JSON");
			grafoCallesYEsquinas = copy;
		}
	}
	public void loadXml(int pEleccion) 
	{	
		String[] Answer;
		String[] argos1= {"data/Proyecto_datos/Central-WashingtonDC-OpenStreetMap.xml"};
		String[] argos3= {"data/Proyecto_datos/map.xml"};

		Answer=pEleccion==1?argos1:pEleccion==2?argos3:null;
		if(Answer!=null)
		{
			xML.loadXml(Answer);
			System.out.println("El grafo se carg�: "+grafoCallesYEsquinas.toString());
			}
		else
			view.printMessage("La opcion de entrada no es valida");
	}
	
	public void generarMapaGrafo(GrafoNoDirigido<String, Coordenadas, Harvesiana> pGrafo,LatLng min,LatLng max)
	{
		Mapa x=new Mapa(grafoCallesYEsquinas,min,max);
		Mapa.graficarMapa(x);
	}
	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha);
	}
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
	/**
	 * Retorna los arcos de una cola pasada por parametro en un arreglo.
	 * @return
	 */
	private NuevoArco<String, Coordenadas, Harvesiana>[] getArregloArcos(Queue<NuevoArco<String, Coordenadas, Harvesiana>> cola)
	{
		IteratorQueue<NuevoArco<String, Coordenadas, Harvesiana>> it=(IteratorQueue<NuevoArco<String, Coordenadas, Harvesiana>>) cola.iterator();
		NuevoArco<String, Coordenadas, Harvesiana>[] infracciones=new NuevoArco[cola.size()];
		int i=0;
		while(it.hasNext())
		{
			NuevoArco<String, Coordenadas, Harvesiana> x=it.next();
			infracciones[i]=x;
			i++;
		}
		return infracciones;
	}
}
