package model.VOMovingViolations;


public class Coordenadas implements Comparable<Coordenadas>
{
	/**
	 * x
	 */
    public Double latitud;
	/**
	 *  y
	 */
    public Double longitud;
  
	/**
	 * Constructor
	 */
	public Coordenadas(Double pLat,Double pLong ) 
	{
		latitud=pLat;
		longitud=pLong;
	}

	
    public Double getLat() 
    {
		return latitud;
	}
	/**
	 * Retorna la coordenada en Y	
	 * @return
	 */
	public Double getLong() 
	{
		return longitud;
	}
	
	
	@Override
	public int compareTo(Coordenadas o) 
	{
		
		int rta=latitud.compareTo(o.latitud);
		if(rta==0)
		{
		   rta=longitud.compareTo(o.longitud);	
		}
		return rta;
	}

	public String toString()
	{
		
		return latitud+","+longitud;
	}
}
