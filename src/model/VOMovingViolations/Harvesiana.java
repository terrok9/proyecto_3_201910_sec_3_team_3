package model.VOMovingViolations;
/**
 * Información y distancias
 */
public class Harvesiana 
{
	
	private String id;
	
	private Double distHarvesiana;

	/**
	 * Constructor
	 */
	public Harvesiana(String pId,Double pDist) 
	{
		id=pId;
		distHarvesiana=pDist;
	}


	public String getId() 
	{
		return id;
	}
   
	
	public void setDistHarvesiana(Double distHarvesiana) 
	{
		this.distHarvesiana = distHarvesiana;
	}
	public Double getDistHarvesiana() 
	{
		return distHarvesiana;
	}
	public void setId(String id) 
	{
		this.id = id;
	}
	
	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return "El id del way: "+id+" ,la distancia harvesiana: "+distHarvesiana;
	}
	
	
}
