package model.VOMovingViolations;
import java.time.LocalDateTime;

public class VOMovingViolations {

	private Integer ObjectId;

	private double xCoord;

	private double yCoord;

	public VOMovingViolations(Integer pObjectId,double pXcoord, double pYcoord){
		ObjectId = pObjectId;
		
		xCoord = pXcoord;
		yCoord = pYcoord;
			
	}

	/**
	 * @return id - Identificador �nico de la infracci�n
	 */
	public Integer objectId() {
		// TODO Auto-generated method stub
		return ObjectId;
	} 
	public double getxCoord() {
		return xCoord;
	}

	public double getyCoord() {
		return yCoord;
	}

	

}

