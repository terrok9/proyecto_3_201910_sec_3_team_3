package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue <T> implements IQueue<T> {
    private int a;         
    private Nodo primero;    
    private Nodo ultimo;     

 
    private class Nodo {
        private T elemento;
        private Nodo siguiente;
    }

   
    public Queue()
    {
        primero = null;
        ultimo  = null;
        a = 0;
        
    }

    
    public boolean isEmpty() {
        return primero == null;
    }

    
    public int size() {
        return a;     
    }

   
    public T peek() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        return primero.elemento;
    }

   
    public void enqueue(T item) {
        Nodo ultimoL = ultimo;
        ultimo = new Nodo();
        ultimo.elemento= item;
        ultimo.siguiente= null;
        if (isEmpty()) primero = ultimo;
        else           ultimoL.siguiente = primero;
        a++;
        
    }

 
    public T dequeue() {
        if (isEmpty()) throw new NoSuchElementException("Queue underflow");
        T x = ultimo.elemento;
        primero = primero.siguiente;
        a--;
        if (isEmpty()) ultimo = null;   
        return x;
    }

 


    public Iterator<T> iterator()  {
        return new ListIterator();  
    }
    private class ListIterator implements Iterator<T> {
        private Nodo actual = primero;

        public boolean hasNext()  { return actual != null;                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T x = actual.elemento;
            actual = actual.siguiente; 
            return x;
        }
        
    }
    public void eliminarElemento(T pElemento)
	{
		// TODO Auto-generated method stub
		Queue<T> colaMitad2=new Queue<T>();
		int tamInicial=a;
        for (int i = 0; i < tamInicial; i++) 
        {
        	T element = dequeue();
        	if(element==pElemento)
        	{
        		colaMitad2.enqueue((T) this);
        		if(colaMitad2.peek()!=null)
        		{
        			this.primero = (Queue<T>.Nodo) colaMitad2.peek();
        			if(this.primero.siguiente == null)
        			{
        				this.primero.siguiente=colaMitad2.ultimo;
        			}
        		}
        		break;
        	}
        	else
        	{
        		colaMitad2.enqueue(element);
        	}
		}		
	}
}