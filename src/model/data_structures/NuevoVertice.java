package model.data_structures;
/**
 * Clase que representa un vertice de la componente de los grafos 
 */
public class NuevoVertice<K extends Comparable<K>,V,A> 
{
	/**
	 * Llave que guarda el vertice
	 */
	private K id;
	/**
	 * Informacion que contiene el vertice
	 */
	private V info;
	/**
	 * Lista de Arcos con vertices adyacentes a este vertice 
	 */
    private Queue<NuevoArco<K,V,A>> adyacentes;
	/**
	 * Constructor del vertice 
	 * @param pLlave. Llave del vertice
	 * @param pValor.  Valor del vertice
	 */
	public NuevoVertice(K pLlave, V pValor)
	{
		id=pLlave;
		info=pValor;
		adyacentes=new Queue<NuevoArco<K,V,A>>();
	}
	/**
	 * Retorna la llave o id del nodo
	 * @return id o llave
	 */
	public K getId() {
		return id;
	}
	/**
	 * Define el id del vertice
	 * @param id o llave a definir
	 */
	public void setId(K id) 
	{
		this.id = id;
	}
	/**
	 * Retorna la informacion del vertice 
	 * @return info del vertice
	 */
	public V getInfo() 
	{
		return info;
	}
	/**
	 * Define la informacion del vertice pasada por parametri
	 * @param info 
	 */
	public void setInfo(V info) 
	{
		this.info = info;
	}
	/**
	 * A�ade un NuevoArco de la lista de adyacentes de un vertice
	 * @param pArco vertice a a�adir en la lista de adyacentes
	 */
	public void agregarArco(NuevoArco<K, V,A> pArco)
	{
		adyacentes.enqueue(pArco); 
	}
	/**
	 * Elimina un arco de la lista de adyacentes de un vertice 
	 * @param pArco vertice a eliminar de la lista
	 */
	public void eliminarArco(NuevoArco<K,V,A> pArco)
	{
		adyacentes.eliminarElemento(pArco);
	}
	/**
	 * Retorna los arcos de la lista de adyacentes
	 */
	public Queue<NuevoArco<K, V, A>> darAdyacentes()
	{
		return adyacentes;
	}
}
