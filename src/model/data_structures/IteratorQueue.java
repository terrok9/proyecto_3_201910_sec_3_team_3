package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * iterador 
 * @author tomado por seweick
 */

public class IteratorQueue<T> implements Iterator<T>
{
	
	private Node<T> proximo;
	
	
	public IteratorQueue(Node<T> primero) 
	{
		// TODO Auto-generated constructor stub
		proximo=primero;
	}

	@Override
	public boolean hasNext() 
	{
		// TODO Auto-generated method stub
		return proximo!=null;
	}

	@Override
	public T next() 
	{
		// TODO Auto-generated method stub
		if ( proximo == null )
		{ 
			throw new NoSuchElementException(); 
		}
		T elemento = proximo.getElemento(); 
		proximo = proximo.getSiguiente(); 
		return elemento;
	}

}
