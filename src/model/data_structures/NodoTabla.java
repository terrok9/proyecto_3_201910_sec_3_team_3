package model.data_structures;

/**
 * NODOS TABLA
 */
public class NodoTabla<K,V> 
{
	/**
	 * value
	 */
	private V value;
	
	/**
	 * key
	 */
	private K key;
	
	/**
	 * Elemento siguiente
	 */
	private NodoTabla<K,V> siguiente;
	

	public NodoTabla(K pLlave, V pValor)
	{
		value = pValor;
		key = pLlave;
	}
	public void setLlave(K pLlave)
    {
    	key = pLlave;
    }
	public NodoTabla(K pLlave, V pValor,NodoTabla<K,V> pSiguiente)
	{
		value = pValor;
		key = pLlave;
		siguiente = pSiguiente;
	}
	
	
	public void setSiguiente(NodoTabla<K,V> pNodo)
    {
    	siguiente = pNodo;
    }
	public K getLlave()
	{
		return key;
	}
	
	public void setValor(V pvalor)
    {
    	value = pvalor;
    }
	
	
	public V getValor()
	{
		return value;
	}
	public NodoTabla<K,V> getSiguiente()
	{
		return siguiente;
	}
	
	
}
