package model.data_structures;
/**
 * Clase que representa un arco del grafo 
 */
public class NuevoArco<T extends Comparable<T>,V,Z> 
{
	/**
	 * Arco
	 */
	private NuevoVertice<T,V,Z> verticeDeInicio;
	
	private NuevoVertice<T,V,Z> verticeAlFinal;
	
	private Z informacion;
	/**
	 * Constructor
	 */
	public NuevoArco(NuevoVertice<T,V,Z> pX1,NuevoVertice<T,V,Z> pX2,Z pInfo){
		verticeDeInicio=pX1;
		verticeAlFinal=pX2;
	}
	
	
	
	public void setInicial(NuevoVertice<T, V,Z> extremo1) {
		this.verticeDeInicio = extremo1;
	}
	
	public NuevoVertice<T, V,Z> darFinal() {
		return verticeAlFinal;
	}
	public NuevoVertice<T,V,Z> darInicial() {
		return verticeDeInicio;
	}
	/**
	 * Define el extremo 2 del arco
	 */
	public void setFinal(NuevoVertice<T, V,Z> extremo2) {
		this.verticeAlFinal = extremo2;
	}
	/**
	 * Retorna la informacion del NuevoArco 
	 */
	public Z darInfoArco() {
		return informacion;
	}
	/**
	 * Define la informacion de una linea
	 */
	public void setInfoArco(Z infoArco) {
		this.informacion = infoArco;
	}

	public String toString() 
	{
		StringBuilder s = new StringBuilder();

		s.append(verticeAlFinal.getId()+ ":" + verticeDeInicio.getId() +":"+informacion);

		return s.toString();
	}
}
