package model.data_structures;
//Autor: el libro
public class LazyPrimMST {
	private boolean[] marked; // MST vertices
	private Queue<NuevoArco> mst; // MST edges
	private Queue<NuevoArco> pq; // crossing (and ineligible) edges
	public LazyPrimMST(GrafoNoDirigido G)
	{
		pq = new Queue<NuevoArco>();
		marked = new boolean[G.V()];
		mst = new Queue<NuevoArco>();
		visit(G, 0); // assumes G is connected (see Exercise 4.3.22)
		while (!pq.isEmpty())
		{
			NuevoArco e = pq.dequeue(); // Get lowest-weight
			NuevoVertice v = e.darInicial();
			int w = e.darFinal(); // edge from pq.
			if (marked[v] && marked[w]) continue; // Skip if ineligible.
			mst.enqueue(e); // Add edge to tree.
			if (!marked[v]) visit(G, v); // Add vertex to tree
			if (!marked[w]) visit(G, w); // (either v or w).
		}
	}
	private void visit(EdgeWeightedGraph G, int v)
	{ // Mark v and add to pq all edges from v to unmarked vertices.
		marked[v] = true;
		for (Edge e : G.adj(v))
			if (!marked[e.other(v)]) pq.insert(e);
	}
	public Iterable<Edge> edges()
	{ return mst; }
	public double weight() // See Exercise 4.3.31.
}
