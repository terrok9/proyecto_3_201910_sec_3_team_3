package model.data_structures;

import java.util.Iterator;

import model.VOMovingViolations.Coordenadas;
import model.VOMovingViolations.VOMovingViolations;

/**
 * 
 * Clase que representa un grafo no dirigido 
 * @author BASADO EN FERNANDO DE LA ROSA Y EL LIBRO
 */
public class GrafoNoDirigido<T extends Comparable<T>,V,B> implements IGrafoNoDirigido <T,V,B> 
{
	
	
	
	private SeparateChainingHashST<T, NuevoVertice<T, V,B>> aristasDelGrafo;
	
	private Queue<NuevoArco<T,V,B>> creacionArcos; 
	
	private int numeroDeVertices;

	private int numeroDeArcos;	
	private ArregloDinamico<T> llaver;
	
	private Queue<VOMovingViolations> infracciones;
	public GrafoNoDirigido() 
	{
		// TODO Auto-generated constructor stub
		aristasDelGrafo=new SeparateChainingHashST<T, NuevoVertice<T,V,B>>(100000);
		numeroDeVertices=0;
		creacionArcos = new Queue<NuevoArco<T,V,B>>();
		llaver = new ArregloDinamico<T>(500);
		numeroDeArcos=0;
		infracciones = new Queue<VOMovingViolations>();
	}
	
	public Queue<NuevoArco<T,V,B>> darArcos()
	{
		return creacionArcos;
	}
	
	public ArregloDinamico<T> keys() 
	{
		return llaver;
	}
  
	public NuevoVertice<T,V,B> darVertice(T idVertex) 
	{
		return aristasDelGrafo.get(idVertex);
	}

	@Override
	public int V() 
	{
		// TODO Auto-generated method stub
		return numeroDeVertices;
	}

	@Override
	public int E() 
	{
		// TODO Auto-generated method stub
		return numeroDeArcos;
	}
	@Override
	public void addVertex(T idVertex, V infoVertex) 
	{
		// TODO Auto-generated method stub
		NuevoVertice<T,V,B> x=new NuevoVertice<T,V,B>(idVertex, infoVertex);
		aristasDelGrafo.put(idVertex, x);
		numeroDeVertices++;
		llaver.agregar(idVertex);
	}
	@Override
	public V getInfoVertex(T idVertex) 
	{
		// TODO Auto-generated method stub
		return aristasDelGrafo.get(idVertex).getInfo();
	}
	@Override
	public void addEdge(T idVertexIni, T idVertexFin, B infoArc) 
	{
		// TODO Auto-generated method stub
		NuevoVertice<T, V,B> pInicial=aristasDelGrafo.get(idVertexIni);
		NuevoVertice<T, V,B> pFinal=aristasDelGrafo.get(idVertexFin);
		NuevoArco<T, V, B> arco=new NuevoArco<T,V,B>(pInicial, pFinal, infoArc);		
		pInicial.agregarArco(arco); 
		pFinal.agregarArco(arco);
		numeroDeArcos++;
		creacionArcos.enqueue(arco);;
	}
	@Override
	public void setInfoVertex(T idVertex, V infoVertex) 
	{
		// TODO Auto-generated method stub
		aristasDelGrafo.get(idVertex).setInfo(infoVertex);
	}
	public String toString() {
		StringBuilder s = new StringBuilder();

		s.append(numeroDeVertices + " vertices, " + numeroDeArcos + " creacionArcos ");

		return s.toString();
	}
	@Override
	public B getInfoArc(T idVertexIni, T idVertexFin) 
	{
		// TODO Auto-generated method stub
		B rta=null;
		Queue<NuevoArco<T, V, B>> adjInicial=aristasDelGrafo.get(idVertexIni).darAdyacentes();
		Iterator<NuevoArco<T, V, B>> it =  adjInicial.iterator();
		while (it.hasNext()) 
		{
			NuevoArco<T, V, B> arco = (NuevoArco<T, V, B>) it.next();
			if(arco.darFinal().equals(aristasDelGrafo.get(idVertexFin)))
			{
				rta=arco.darInfoArco();
				break;
			}
		}
		return rta;
	}
    public int getNumeroInfracciones(T idVertexIni, T idVertexFin){
    	int retorno = 0;
    	retorno += infracciones.size();
    	return retorno;
    }
	@Override
	public void setInfoArc(T idVertexIni, T idVertexFin, B infoArc) 
	{
		// TODO Auto-generated method stub
		Queue<NuevoArco<T, V, B>> adjInicial=aristasDelGrafo.get(idVertexIni).darAdyacentes();
		Iterator<NuevoArco<T, V, B>> it =  adjInicial.iterator();
		while (it.hasNext()) 
		{
			NuevoArco<T, V, B> arco = (NuevoArco<T, V, B>) it.next();
			if(arco.darFinal().equals(aristasDelGrafo.get(idVertexFin)))
			{
				arco.setInfoArco(infoArc);
				break;
			}
		}
	}
    public void agregarInfracciones(Coordenadas infoVertex, Coordenadas infoVertexFin, Queue<VOMovingViolations> pInfracciones){
    	VOMovingViolations a = pInfracciones.dequeue();
    	if( infoVertex.getLong() <= a.getxCoord() && infoVertex.getLat() <= a.getyCoord() && infoVertexFin.getLong() >= a.getxCoord() && infoVertexFin.getLat() >= a.getyCoord()){
    		infracciones.enqueue(a);
    	}
    }
	@Override
	public IteratorQueue<T> adj(T idVertex) 
	{
		// TODO Auto-generated method stub
		Queue<T> colaRta=new Queue<T>();
		Queue<NuevoArco<T, V, B>> adjInicial=aristasDelGrafo.get(idVertex).darAdyacentes();
		Iterator<NuevoArco<T, V, B>> it=  adjInicial.iterator();
		while (it.hasNext()) 
		{
			NuevoArco<T, V, B> arco = (NuevoArco<T, V, B>) it.next();
			colaRta.enqueue(arco.darFinal().getId());
		}
		return (IteratorQueue<T>) colaRta.iterator();
	}


}
