package model.data_structures;

/**
 * NODOS
 */
public class Node<T> 
{
	
	private T elemento;
	
	private Node<T> siguiente;
	
	
	
	public Node(T dato,Node<T> pSiguiente)
	{
		elemento=dato;
		siguiente=pSiguiente;
	}
	
	
	public Node(T dato)
	{
		elemento=dato;
	}
	
	
	
	public Node<T> getSiguiente()
	{
		return siguiente;
	}
	
	public void setSiguiente(Node<T> pNodo)
    {
    	siguiente=pNodo;
    }
	public T getElemento()
	{
		return elemento;
	}
	public void setElemento(T pElemento)
    {
    	elemento=pElemento;
    }
	
}
