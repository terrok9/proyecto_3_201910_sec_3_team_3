package Mapa;

import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.VOMovingViolations.Coordenadas;
import model.VOMovingViolations.Harvesiana;
import model.data_structures.NuevoArco;
import model.data_structures.ArregloDinamico;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Queue;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.LatLngBounds;
import com.teamdev.jxmaps.CircleOptions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
/**
 * Clase que representa el cartografo
 */
public class Mapa extends MapView
{
	/**
	 * Grafo a graficar en el cartografo
	 */
	private GrafoNoDirigido<String, Coordenadas, Harvesiana> cartografo;
	/**
	 * Contructor de la clase
	 * @param pMapa
	 * @author basado en el ejemplo en xerces
	 */
	public Mapa(GrafoNoDirigido<String, Coordenadas, Harvesiana> pMapa,LatLng min,LatLng max) 
	{
		cartografo = pMapa;
		setOnMapReadyHandler(new MapReadyHandler() 
		
		{
			@Override
			public void onMapReady(MapStatus status) 
			{
				
				if (status == MapStatus.MAP_STATUS_OK) 
				{
					ArrayList<Circle> circunferencias = new ArrayList<Circle>();
					CircleOptions op = new CircleOptions();
					op.setFillColor("#1C77D6");
					op.setStrokeColor("#1C55D6");
					
					op.setRadius(2);
					
					int n = 0;
					final Map map = getMap();
					
				
					ArrayList<Polyline> lineas = new ArrayList<Polyline>();
					Iterator<NuevoArco<String, Coordenadas, Harvesiana>> colaArcos = cartografo.darArcos().iterator();
					
					PolylineOptions options = new PolylineOptions();
					options.setStrokeOpacity(1.0);
					
					options.setStrokeWeight(2.0);
					options.setGeodesic(true);
					
					options.setStrokeColor("#36S71C");
					
					
					
					
					
					
					while(colaArcos.hasNext()) 
					{
						NuevoArco<String, Coordenadas, Harvesiana> arc = colaArcos.next();
						double lat1 = cartografo.getInfoVertex(arc.darInicial().getId()).getLat();
						
						double long2 = cartografo.getInfoVertex(arc.darFinal().getId()).getLong();
						double long1 = cartografo.getInfoVertex(arc.darInicial().getId()).getLong();
						double lat2 = cartografo.getInfoVertex(arc.darFinal().getId()).getLat();
						
						if(n <5000&& lat1 >= min.getLat() && lat1 <= max.getLat() && long1 >= min.getLng() && long1 <= max.getLng()
								&& lat2 >= min.getLat() && lat2 <= max.getLat() && long2 >= min.getLng() && long2 <= max.getLng()) 
						{
							LatLng[] path = {new LatLng(lat1,long1),new LatLng(lat2,long2)};
							lineas.add(new Polyline(map));
							lineas.get(lineas.size()-1).setPath(path);
							lineas.get(lineas.size()-1).setOptions(options);
							circunferencias.add(new Circle(map));
							circunferencias.get(circunferencias.size()-1).setCenter(new LatLng(lat1,long1));
							circunferencias.get(circunferencias.size()-1).setOptions(op);
							circunferencias.add(new Circle(map));
							circunferencias.get(circunferencias.size()-1).setCenter(new LatLng(lat2,long2));
							circunferencias.get(circunferencias.size()-1).setOptions(op);
							n++;
						}
					}
					System.out.println("Numero de arcos graficados:: "+n);
					map.fitBounds(new LatLngBounds(min,max));
					// Setting initial zoom value
					map.setZoom(14);
				}
			}
		});
	}

	public static void graficarMapa(Mapa sample)
	{
		JFrame frame = new JFrame("GrafoFinAL");

		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(sample, BorderLayout.CENTER);
		frame.setSize(1000, 750);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}