package main;

import controller.Controller;

public class MVC 
{
	/**
	 * Clase que inicializa la app
	 */
	public static Controller controler ;
	/**
	 * M�todo que inicia la app
	 */
	public static void main(String[] args)
	{
		controler = new Controller();
		controler.run();
	}
}
